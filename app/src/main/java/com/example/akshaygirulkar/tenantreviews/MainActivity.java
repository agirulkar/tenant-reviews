package com.example.akshaygirulkar.tenantreviews;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.example.akshaygirulkar.tenantreviews.database.UserDatabase;
import com.example.akshaygirulkar.tenantreviews.database.model.User;
import com.example.akshaygirulkar.tenantreviews.network.CheckNetworkState;
import com.example.akshaygirulkar.tenantreviews.network.NetworkController;
import org.json.JSONObject;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.akshaygirulkar.tenantreviews.database.Constant.LOGGEDIN_KEY;
import static com.example.akshaygirulkar.tenantreviews.database.Constant.LOGIN_APPLICATION;
import static com.example.akshaygirulkar.tenantreviews.network.UrlConstants.GET_URL_NODE;
import static com.example.akshaygirulkar.tenantreviews.network.UrlConstants.GET_URL_REQUEST_CODE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NetworkController.ResultListener{

    long backPressedTime;
    Toast backPressed;
    UserDatabase userDatabase;
    SharedPreferences sp;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        sp = getSharedPreferences(LOGIN_APPLICATION,MODE_PRIVATE);
        coordinatorLayout = findViewById(R.id.coordinatorlayout);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        displaySelectedScreen(R.id.nav_search_review);

        userDatabase = userDatabase.getDatabase(MainActivity.this);
        new RetrieveTask(MainActivity.this).execute();

        NetworkController.getInstance().connect(this,GET_URL_NODE,GET_URL_REQUEST_CODE,null,this);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(!CheckNetworkState.isOnline(MainActivity.this)){
                    showSnackBar();
                }
            }
        },0,20*1000);
    }

    private void showSnackBar(){
        Snackbar snackbar;
        snackbar = Snackbar.make(coordinatorLayout,"No Internet. Please try again with Internet",Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(Color.parseColor("#2874F0"));
        snackbar.show();
    }

    @Override
    public void onResult(String url, boolean isSuccess, JSONObject jsonObject, VolleyError volleyError, ProgressDialog progressDialog) {
        System.out.println("Url is ====>"+url);
        if(isSuccess) {
                System.out.println("API Call Integration====>"+jsonObject.toString());
        }
        else
            Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG).show();

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if(backPressedTime + 2000 > System.currentTimeMillis()){
                backPressed.cancel();
                finishAffinity();
                super.onBackPressed();
                return;
            }else{
                backPressed = Toast.makeText(getApplicationContext(),"Double press exit",Toast.LENGTH_SHORT);
                backPressed.show();
            }
            backPressedTime = System.currentTimeMillis();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_search_review:
                fragment = new SearchTenant();
                break;
            case R.id.nav_add_review:
                fragment = new AddReview();
                break;
            case R.id.nav_log_out:
                sp.edit().putBoolean(LOGGEDIN_KEY,false).apply();
                Intent changeActivity = new Intent(MainActivity.this,LoginActivity.class);
                MainActivity.this.startActivity(changeActivity);
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }




    private class RetrieveTask extends AsyncTask<Void,Void,List<User>>{


        private WeakReference<MainActivity> weakReference;

        RetrieveTask(MainActivity context){
            this.weakReference = new WeakReference<>(context);
        }

        @Override
        protected List<User> doInBackground(Void... voids) {
            if(weakReference.get()!= null) {
                return weakReference.get().userDatabase.getUserDao().getUser();
            }
            else return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(List<User> userList){
            TextView headerName = findViewById(R.id.header_name);
            TextView headerEmail = findViewById(R.id.header_email);
            if(userList != null && userList.size()>0){
                userList.forEach(user -> {
                    headerName.setText(user.getName());
                    headerEmail.setText(user.getEmail());
                });
            }
        }
    }
}
