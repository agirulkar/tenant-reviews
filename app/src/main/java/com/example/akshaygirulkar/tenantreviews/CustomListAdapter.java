package com.example.akshaygirulkar.tenantreviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;


public class CustomListAdapter extends BaseAdapter {
    LayoutInflater inflater;
    String[] LIST_HEADERS = {"Too Bad","Worst","Good","Pathetic","Too Bad","Worst","Simply awesome","Noisy untalented","Bad Experience","Pathetic"};
    String[] LIST_DESC    = {"Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved",
            "Cap is comfortable and looking good .. Problems is with colour of the cap . with single wash it has became whitish .. So its not worthy to pay this much amount for this cap .. Quality needs to be improved"};
    Integer[] LIST_RATING  = {1,2,3,1,2,1,5,2,4,2};

    public CustomListAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }
    @Override
    public int getCount() {
        return LIST_HEADERS.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.custom_list,null);
        TextView textHeader = view.findViewById(R.id.textHeader);
        TextView textDesc = view.findViewById(R.id.textDesc);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        ratingBar.setRating(LIST_RATING[i]);
        textHeader.setText(LIST_HEADERS[i]);
        textDesc.setText(LIST_DESC[i]);
        return view;
    }

}
