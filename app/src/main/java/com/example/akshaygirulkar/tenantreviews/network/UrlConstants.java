package com.example.akshaygirulkar.tenantreviews.network;

public interface UrlConstants {

    String GET_URL_PHP = "http://androiderstack.com/api/getDays.php";
    String GET_URL_NODE = "https://jsonplaceholder.typicode.com/posts/";
    String POST_URL = "http://androiderstack.com/api/postAPI.php";

    int GET_URL_REQUEST_CODE = 1;
    int POST_URL_REQUEST_CODE = 2;
}
