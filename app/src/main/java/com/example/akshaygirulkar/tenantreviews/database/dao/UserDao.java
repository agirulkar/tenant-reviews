package com.example.akshaygirulkar.tenantreviews.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.akshaygirulkar.tenantreviews.database.Constant;
import com.example.akshaygirulkar.tenantreviews.database.model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM "+Constant.TABLE_NAME)
    List<User> getUser();

    @Insert
    long insertUser(User user);

    @Update
    void updateUser(User user);

    @Delete
    void deleteUser(User user);

}
