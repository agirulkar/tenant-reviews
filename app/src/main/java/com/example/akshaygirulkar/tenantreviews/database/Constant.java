package com.example.akshaygirulkar.tenantreviews.database;

public class Constant {
    public static final String  TABLE_NAME = "user";
    public static final String  DATABASE_NAME = "user_db.db";
    public static final String  LOGIN_APPLICATION = "login";
    public static final String  MODE_PRIVATE = "MODE_PRIVATE";
    public static final String  LOGGEDIN_KEY = "LOGGEDIN";
}
