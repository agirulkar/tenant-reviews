package com.example.akshaygirulkar.tenantreviews.database.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import com.example.akshaygirulkar.tenantreviews.database.Constant;
import java.io.Serializable;
import java.util.Date;

@Entity(tableName = Constant.TABLE_NAME)
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long userId;

    private String name;
    private String email;
    private String phone;
    private String password;

    public User(String name, String email, String phone, String password){
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    @Ignore
    public User(){}


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
