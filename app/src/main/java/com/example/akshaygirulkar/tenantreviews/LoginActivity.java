package com.example.akshaygirulkar.tenantreviews;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.akshaygirulkar.tenantreviews.database.UserDatabase;
import com.example.akshaygirulkar.tenantreviews.database.model.User;
import java.lang.ref.WeakReference;
import java.util.List;
import static com.example.akshaygirulkar.tenantreviews.database.Constant.LOGGEDIN_KEY;
import static com.example.akshaygirulkar.tenantreviews.database.Constant.LOGIN_APPLICATION;

public class LoginActivity extends AppCompatActivity {
    TextView signUp;
    TextView forgotPassword;
    long backPressedTime;
    Toast backPressed;
    UserDatabase userDatabase;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signUp = findViewById(R.id.create_one);
        forgotPassword = findViewById(R.id.forgot_password);
        sp = getSharedPreferences(LOGIN_APPLICATION,MODE_PRIVATE);
        userDatabase = userDatabase.getDatabase(LoginActivity.this);

        new RetrievCredentials(LoginActivity.this).execute();

        //sharedPreferences check for LoggedIn status. This will give us true. Notice that false is the default value for Boolean
        if(sp.getBoolean(LOGGEDIN_KEY,false)){
            Intent activityChange = new Intent(LoginActivity.this,MainActivity.class);
            LoginActivity.this.startActivity(activityChange);
        }

        signUp.setText(Html.fromHtml(getString(R.string.create_one)));
        signUp.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent activityChange = new Intent(LoginActivity.this,SignUpActivity.class);
                LoginActivity.this.startActivity(activityChange);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activityChange = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                LoginActivity.this.startActivity(activityChange);
            }
        });
    }
    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()){
            backPressed.cancel();
            finishAffinity();
            super.onBackPressed();
            return;
        }else{
            backPressed = Toast.makeText(getApplicationContext(),"Double press exit",Toast.LENGTH_SHORT);
            backPressed.show();
        }
        backPressedTime = System.currentTimeMillis();

    }

    private class RetrievCredentials extends AsyncTask<Void,Void,List<User>>{

        private WeakReference<LoginActivity> weakReference;

        RetrievCredentials(LoginActivity context){
            this.weakReference = new WeakReference<>(context);
        }

        @Override
        protected List<User> doInBackground(Void... voids) {
            if(weakReference.get()!=null)
                return weakReference.get().userDatabase.getUserDao().getUser();
            else
                return null;
        }

        @Override
        protected void onPostExecute(List<User> userList){
            Button loginButton = findViewById(R.id.login_button);
            EditText userEmail = findViewById(R.id.user_email);
            EditText userPassword = findViewById(R.id.password);

            loginButton.setOnClickListener(new View.OnClickListener() {

                @SuppressLint("NewApi")
                @Override
                public void onClick(View view) {
                    if(userList!=null && userList.size()>0){
                        userList.forEach(user -> {
                            if(userEmail.getText().toString().equals(user.getEmail()) && userPassword.getText().toString().equals(user.getPassword())){
                                sp.edit().putBoolean(LOGGEDIN_KEY,true).apply();
                                Intent activityChange = new Intent(LoginActivity.this,MainActivity.class);
                                LoginActivity.this.startActivity(activityChange);
                            }
                            else {
                                Toast.makeText(getApplicationContext(),"Email Or Password is Wrong",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else{
                        // userList size is 0 or db not created yet.
                        Toast.makeText(getApplicationContext(),"User Not Created. Sign Up first",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
