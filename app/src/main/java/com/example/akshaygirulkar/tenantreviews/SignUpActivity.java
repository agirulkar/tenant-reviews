package com.example.akshaygirulkar.tenantreviews;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.akshaygirulkar.tenantreviews.database.UserDatabase;
import com.example.akshaygirulkar.tenantreviews.database.model.User;

import java.lang.ref.WeakReference;

public class SignUpActivity extends AppCompatActivity {
    Button signUpButton;
    TextView logIn;
    EditText userName;
    EditText userEmail;
    EditText userPhone;
    EditText userPassword;
    UserDatabase userDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        userDatabase = userDatabase.getDatabase(SignUpActivity.this);
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);
        userPhone = findViewById(R.id.user_phone);
        userPassword = findViewById(R.id.password);
        signUpButton = findViewById(R.id.sign_up_button);
        logIn = findViewById(R.id.log_in);

        logIn.setText(Html.fromHtml(getString(R.string.log_in)));
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeActivity = new Intent(SignUpActivity.this,LoginActivity.class);
                SignUpActivity.this.startActivity(changeActivity);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Name is =====>"+userName.getText().toString());
                System.out.println("Email is =====>"+userEmail.getText().toString());
                System.out.println("Phone is =====>"+userPhone.getText().toString());
                System.out.println("Password is =====>"+userPassword.getText().toString());

                User user = new User(userName.getText().toString(),userEmail.getText().toString(),userPhone.getText().toString(),userPassword.getText().toString());
                new InsertTask(SignUpActivity.this,user).execute();

                Intent changeActivity = new Intent(SignUpActivity.this,LoginActivity.class);
                SignUpActivity.this.startActivity(changeActivity);
                Toast.makeText(getApplicationContext(),"User created",Toast.LENGTH_LONG).show();
            }
        });
    }

    private class InsertTask extends AsyncTask<Void,Void,Boolean>{

        private User user;
        private WeakReference<SignUpActivity> weakReference;

        InsertTask(SignUpActivity context,User user){
            this.user = user;
            this.weakReference = new WeakReference<>(context);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            long id = weakReference.get().userDatabase.getUserDao().insertUser(user);
            user.setUserId(id);
            Log.e("User Id","doInBackground "+id);
            System.out.println("Doinbackground user inserted=======>"+id);
            return null;
        }
    }
}
