package com.example.akshaygirulkar.tenantreviews.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.akshaygirulkar.tenantreviews.database.dao.UserDao;
import com.example.akshaygirulkar.tenantreviews.database.model.User;


@Database(entities = {User.class} , version = 1)
public abstract class UserDatabase extends RoomDatabase {

    private static UserDatabase INSTANCE;

    public abstract UserDao getUserDao();

    public static UserDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (UserDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            UserDatabase.class, Constant.DATABASE_NAME)
                            .allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }

    public void cleanUp(){
        INSTANCE = null;
    }
}
