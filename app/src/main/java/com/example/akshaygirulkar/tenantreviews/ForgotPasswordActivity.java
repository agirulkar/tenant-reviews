package com.example.akshaygirulkar.tenantreviews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ForgotPasswordActivity extends AppCompatActivity {
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        submit = findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeActivity = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                ForgotPasswordActivity.this.startActivity(changeActivity);
                Toast.makeText(getApplicationContext(),
                        "Forgot Password Link sent to Email ID",
                        Toast.LENGTH_LONG).show();

            }
        });
    }
}
