package com.example.akshaygirulkar.tenantreviews;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import static android.text.InputType.TYPE_CLASS_PHONE;

public class SearchTenant extends Fragment {
   ListView listView;
   SearchView searchView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_tenant,container,false);
        listView = view.findViewById(R.id.listInfo);
        searchView = view.findViewById(R.id.searchView);
        searchView.setInputType(TYPE_CLASS_PHONE);
        listView.setAdapter(new CustomListAdapter(inflater));
        Log.e("Search Value"," "+searchView.getQuery());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Search Tenant");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.e("Search Submit"," "+s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.e("Search textchange"," "+s);
                return false;
            }
        });
    }


}
